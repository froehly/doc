* How to properly use the platform
:PROPERTIES:
:CUSTOM_ID: usage
:END:
The user's charter for the use of the platform is available
[[https://www.plafrim.fr/wp-content/uploads/2015/09/2015_09_charte_plafrim_en.pdf]].

Here an excerpt with the good usage rules
- Please try to plan large scale experiments during night-time or
  holidays.
- Between 09:00 and 19:00 (local time of the cluster) during working
  days, you should not use more than the equivalent of 5 hours on all
  the cores of a sub-cluster (for instance bora) during a given day.
  e.g. On a 68 bi-processor (quadri cores) cluster, you should not use more than
  5*2*4*68 = 2720h between 09:00 and 19:00 CEST for a potential amount
  of 5440h during this period). Extending an overnight reservation to
  include this daily quota is considered rude, as you already had your
  fair share of platform usage with the overnight reservation.
- You should not have more than 3 reservations in advance, because it
  kills down resource usage. Please optimize and submit jobs instead.
- You should not have jobs that last more than 72h outside week-end,
  even for a small amount of nodes, as this increases fragmentation of
  the platform.

If you need to run many large/long jobs, you should consider moving
to a larger production cluster such as a mesocentre. Please remember
that PlaFRIM is an experimentation platform.

See also [[https://plafrim-users.gitlabpages.inria.fr/doc/#slurm_flooding][how to avoid flooding the platform with too many jobs]].
