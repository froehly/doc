* Hardware Documentation
:PROPERTIES:
:CUSTOM_ID: hardware
:END:

PlaFRIM aims to allow users to experiment with new hardware technologies and to develop new codes.

Access to the cluster state (sign-in required) : [[https://www.plafrim.fr/jobs-monitoring/][Pistache]]

You will find below a list of all the available nodes on the platform by category.

To allocate a specific category of node with SLURM, you need to specify the node features. To display the list, call the command

#+begin_src sh :eval never-export
$ sinfo -o "%60f %N" -S N
AVAIL_FEATURES                                               NODELIST
arm,cavium,thunderx2                                         arm01
bora,intel,cascadelake,omnipath                              bora[001-044]
brise,intel,broadwell,bigmem                                 brise
amd,zen2,diablo,mellanox                                     diablo[01-04]
amd,zen2,diablo,bigmem,mellanox                              diablo05
amd,zen3,diablo,bigmem,mellanox                              diablo[06-09]
amd,zen4,enbata,mellanox,amdgpu,mi210                        enbata[01-02]
kona,intel,knightslanding,knl,omnipath                       kona[01-04]
sirocco,intel,broadwell,omnipath,nvidia,tesla,p100           sirocco[07-13]
sirocco,intel,skylake,omnipath,nvidia,tesla,v100             sirocco[14-16]
sirocco,intel,skylake,omnipath,nvidia,tesla,v100,bigmem      sirocco17
sirocco,intel,skylake,nvidia,quadro,rtx8000                  sirocco[18-20]
sirocco,amd,zen2,nvidia,ampere,a100                          sirocco21
sirocco,amd,zen3,nvidia,ampere,a100                          sirocco[22-25]
suet,intel,icelake,intelgpu,gpuflex,flex170                  suet01
souris,sgi,ivybridge,bigmem                                  souris
visu                                                         visu01
amd,zen2,zonda                                               zonda[01-21]
#+end_src

For example, to reserve a bora node, you need to call
#+begin_src sh :eval never-export
$ salloc -C bora
#+end_src

To reserve a sirocco node with V100 GPUs, you need to call
#+begin_src sh :eval never-export
$ salloc -C "sirocco&v100"
#+end_src

To reserve a big-memory (1TB) diablo node:
#+begin_src sh :eval never-export
$ salloc -C "diablo&bigmem"
#+end_src

** Overview
:PROPERTIES:
:CUSTOM_ID: overview
:END:

|-------------------+-------------------------+-------------+-----------------+--------------------|
|                   | CPU                     | Memory      | GPU             | Storage            |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*bora001-044][bora001-044]]       | 2x 18-core Intel CascadeLake | 192GB       |                 | /tmp of 1 To       |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*diablo01-09][diablo01-04]]     | 2x 32-core AMD Zen2 | 256 GB      |                 | /tmp of 1 TB       |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*diablo01-09][diablo05]]         | 2x 64-core AMD Zen2 | 1 TB        |                 | /tmp of 1 TB       |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*diablo01-09][diablo06-09]]     | 2x 64-core AMD Zen3 | 1 TB        |                 | /scratch of 4 TB       |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*zonda01-21][zonda01-21]]        | 2x 32-core AMD Zen2 | 256 GB      |                 |                    |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*arm01][arm01]]             | 2x 28-core ARM TX2 | 256 GB      |                 | /tmp of 128 GB     |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*sirocco07-13 with 2 NVIDIA P100 GPUs][sirocco07-13]]      | 2x 16-core Intel Broadwell | 256 GB      | 2 NVIDIA P100   | /tmp of 300 GB     |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*sirocco14-16 with 2 NVIDIA V100 GPUs and a NVMe disk][sirocco14-16]]      | 2x 16-core Intel Skylake | 384 GB      | 2 NVIDIA V100   | /scratch of 750 GB |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*sirocco17 with 2 NVIDIA V100 GPUs and 1TB memory][sirocco17]]         | 2x 20-core Intel Skylake | 1 TB        | 2 NVIDIA V100   | /tmp of 1 TB       |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*sirocco18-20 with 2 NVIDIA Quadro RTX8000 GPUs][sirocco18-20]]      | 2x 20-core Intel CascadeLake | 192 GB      | 2 NVIDIA Quadro |                    |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*sirocco21 with 2 NVIDIA A100 GPUs][sirocco21]]         | 2x 24-core AMD Zen2 | 512 GB      | 2 NVIDIA A100   | /scratch of 3.5 TB |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*sirocco22-25 with 2 NVIDIA A100 GPUs][sirocco22-25]]   | 2x 32-core AMD Zen3 | 512 GB      | 2 NVIDIA A100   | /scratch of 4 TB   |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*enbata01-02 with 2 AMD MI210 GPUs][enbata01-02]]       | 2x 32-core AMD Zen4 | 384 GB | 2 AMD MI210 | |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*suet01 with 2 Intel Flex 170 GPUs][suet01]]            | 2x 28-core Intel IceLake | 256 GB | 2 Intel DataCenter GPU Flex 170 | /scratch of 1 TB |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*kona01-04 Knights Landing Xeon Phi][kona01-04]]         | 64-core Intel Xeon Phi | 96GB + 16GB |                 | /scratch of 800 GB |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*brise with 4 sockets, 96 cores and 1 TB memory][brise]]             | 4x 24-core Intel Broadwell | 1TB         |                 | /tmp of 280 GB     |
|-------------------+-------------------------+-------------+-----------------+--------------------|
| [[*souris (SGI Altix UV2000) with 12 sockets, 96 cores and 3 TB memory][souris]]            | 12x 8-core Intel IvyBridge | 3TB         |                 |                    |
|-------------------+-------------------------+-------------+-----------------+--------------------|


*** Network Overview
:PROPERTIES:
:CUSTOM_ID: network_overview
:END:

All nodes are connected through a 10Gbit/s Ethernet network that may also be used for the BeeGFS storage.
The only exception are kona nodes since they only have 1Gbit/s Ethernet.

Additional HPC networks are available between some nodes:

- *OmniPath 100Gbit/s* (3 separate networks):

  - *bora nodes* (and devel[01-02]), also used for BeeGFS storage.

  [[file:./figures/omnipath-bora.png]]

  - *sirocco[07-17]* and all *kona* only

- *Mellanox InfiniBand HDR 200Gbit/s* between *diablo[01-09]*.

  [[file:./figures/infiniband-diablo.png]]

** Standard nodes
:PROPERTIES:
:CUSTOM_ID: standard_nodes
:END:
*** bora001-044
:PROPERTIES:
:CUSTOM_ID: bora001-044
:END:

- CPU :: 2x 18-core Cascade Lake Intel Xeon Skylake Gold 6240 @ 2.6 GHz
  ([[https://en.wikichip.org/wiki/intel/xeon_gold/6140][CPU specs]]).

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-bora.png]]


- Memory :: 192 GB (5.3 GB/core) @ 2933 MT/s.

- Network ::
  OmniPath 100 Gbit/s.

  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/tmp) of 1 To (SATA Seagate ST1000NX0443 @ 7.2krpm).

  BeeGFS over 100G OmniPath.

*** diablo01-09
:PROPERTIES:
:CUSTOM_ID: diablo01-09
:END:

- CPU ::
  2x 32-core AMD Zen2 EPYC 7452 @ 2.35 GHz on diablo01-04 ([[https://en.wikichip.org/wiki/amd/epyc/7452][CPU specs]]).

  [[file:./figures/lstopo-diablo01.png]]

  2x 64-core AMD Zen2 EPYC 7702 @ 2 GHz on diablo05 ([[https://en.wikichip.org/wiki/amd/epyc/7702][CPU specs]]).

  [[file:./figures/lstopo-diablo05.png]]

  2x 64-core AMD Zen3 EPYC 7763 @ 2.45 GHz on diablo06-09 ([[https://en.wikichip.org/wiki/amd/epyc/7763][CPU specs]]).

  [[file:./figures/lstopo-diablo06.png]]

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

- Memory ::

  256 GB (4 GB/core, with a single 128GB NUMA node per CPU) @ 2133 MT/s (diablo01-04).

  1 TB (8 GB/core, with four 128GB NUMA nodes per CPU) @2133 MT/s (diablo05).

  1 TB (8 GB/core, with a single 512GB NUMA node per CPU) @3200 MT/s (diablo06-09)

- Network ::

  Mellanox InfiniBand HDR 200 Gbit/s.

  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/tmp) of 1 TB (SATA Seagate ST1000NM0008-2F2 @ 7.2krpm) (diablo01-05).

  Local disk (/scratch) of 4 TB (RAID0 of 4 HPE SATA-6G HDD @ 7.2krpm) (diablo06-09).

  BeeGFS over 10G Ethernet.

*** zonda01-21
:PROPERTIES:
:CUSTOM_ID: zonda01-21
:END:

- CPU ::
  2x 32-core AMD Zen2 EPYC 7452 @ 2.35 GHz ([[https://en.wikichip.org/wiki/amd/epyc/7452][CPU specs]]).

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-zonda01.png]]

- Memory ::
  256 GB (4 GB/core) @ 3200 MT/s.

- Network ::
  10 Gbit/s Ethernet.

- Storage ::
  BeeGFS over 10G Ethernet.

*** arm01
:PROPERTIES:
:CUSTOM_ID: arm01
:END:

- CPU ::
  2x 28-core ARM Cavium ThunderX2 CN9975 v2.1 @ 2.0 GHz ([[https://en.wikichip.org/wiki/cavium/thunderx2][CPU specs]]).

  By default, Turbo-Boost is disabled to ensure the reproducibility of the experiments carried out on the nodes.

  However Hyperthreading is enabled on this node.

  [[file:./figures/lstopo-arm01.png]]

- Memory ::
  256GB (4.6 GB/core) @ 2666 MT/s.

- Network ::
  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/tmp) of 128 GB (SATA Seagate ST1000NM0008-2F2 @ 7.2krpm).

  BeeGFS over 10G Ethernet.

** Accelerated nodes
:PROPERTIES:
:CUSTOM_ID: accelerated_nodes
:END:

None of the nodes with GPUs have NVLink. There are nodes with NVLink in
[[https://plafrim-users.gitlabpages.inria.fr/doc/#other][Grid5000]].

*** Which CUDA module to load?
:PROPERTIES:
:CUSTOM_ID: cuda
:END:

@@html:<red>@@This step is currently unnecessary on PlaFRIM since old sirocco were removed.@@html:</red>@@

Some old sirocco nodes have old GPUs that do not work with CUDA12. If
you load the CUDA 12 module there, it will fail to work with the CUDA
11 driver. An easy way to load the appropriate cuda module is:

#+begin_src sh :eval never-export
cudaversion=$(nvidia-smi -q | grep CUDA | awk {'print $4'})
module load compiler/cuda/$cudaversion
#+end_src

*** sirocco07-13 with 2 NVIDIA P100 GPUs
:PROPERTIES:
:CUSTOM_ID: sirocco07-13
:END:

- CPU :: 2x 16-core Broadwell Intel Xeon E5-2683 v4 @ 2.1 GHz
  ([[https://en.wikichip.org/wiki/intel/microarchitectures/broadwell_(client)][Broadwell specs]]).

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-sirocco07.png]]

- Memory ::
  256 GB (8GB/core) @ 2133 MT/s.

- GPUs ::
  2 NVIDIA P100 (16GB).

- Network ::
  Omnipath 100 Gbit/s.

  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/tmp) of 300 GB (SAS WD Ultrastar HUC156030CSS204 @ 15krpm).

  BeeGFS over 10G Ethernet.

*** sirocco14-16 with 2 NVIDIA V100 GPUs and a NVMe disk
:PROPERTIES:
:CUSTOM_ID: sirocco14-16
:END:

- CPU :: 2x 16-core Skylake Intel Xeon Gold 6142 @ 2.6 GHz
  ([[https://en.wikichip.org/wiki/intel/xeon_gold/6142][CPU specs]]).

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-sirocco14.png]]

- Memory ::
  384 GB (12 GB/core) @ 2666 MT/s.

- GPUs ::
  2 NVIDIA V100 (16GB).

- Network ::
  Omnipath 100 Gbit/s.

  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/scratch) of 750 GB (NVMe Samsung).

  BeeGFS over 10G Ethernet.

*** sirocco17 with 2 NVIDIA V100 GPUs and 1TB memory
:PROPERTIES:
:CUSTOM_ID: sirocco17
:END:

- CPU :: 2x 20-core Skylake Intel Xeon Gold 6148 @ 2.4GHz
  ([[https://en.wikichip.org/wiki/intel/xeon_gold/6148][CPU specs]]).

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-sirocco17.png]]

- Memory ::
  1 TB (25.6 GB/core) @ 1866 MT/s.

- GPUs ::
  2 NVIDIA V100 (16GB).

- Network ::
  Omnipath 100 Gbit/s.

  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/tmp) of 1 TB (SAS Seagate ST300MP0026 @ 15krpm).

  BeeGFS over 10G Ethernet.

*** sirocco18-20 with 2 NVIDIA Quadro RTX8000 GPUs
:PROPERTIES:
:CUSTOM_ID: sirocco18-20
:END:

- CPU :: 2x 20-core Cascade Lake Intel Xeon Gold 5218R CPU @ 2.10 GHz
  ([[https://en.wikichip.org/wiki/intel/xeon_gold/5218r][CPU specs]]).

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-sirocco18.png]]

- Memory ::
  192 GB (4.8GB/core) @ 3200 MT/s.

- GPUs ::
  2 NVIDIA Quadro RTX8000 (48GB).

- Network ::
  10 Gbit/s Ethernet.

- Storage ::
  No local storage.

  BeeGFS over 10G Ethernet.

*** sirocco21 with 2 NVIDIA A100 GPUs
:PROPERTIES:
:CUSTOM_ID: sirocco21
:END:

- CPU :: 2x 24-core AMD Zen2 EPYC 7402 @ 2.80 GHz
  ([[https://en.wikichip.org/wiki/amd/epyc/7402][CPU specs]]).

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-sirocco21.png]]

- Memory ::
  512GB (10.6GB/core) @ 3200 MT/s.

- GPUs ::
  2 NVIDIA A100 (40GB).

- Network ::
  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/scratch) of 3.5 TB (RAID0 of 2 SAS SSD TOSHIBA KRM5XVUG1T92 Rm5 Mixed Use).

  BeeGFS over 10G Ethernet.

*** sirocco22-25 with 2 NVIDIA A100 GPUs
:PROPERTIES:
:CUSTOM_ID: sirocco22-25
:END:

- CPU :: 2x 32-core AMD Zen3 EPYC 7513 @ 2.60 GHz
  ([[https://en.wikichip.org/wiki/amd/epyc/7513][CPU specs]]).

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-sirocco22.png]]

- Memory ::
  512GB (8GB/core) @ 3200 MT/s.

- GPUs ::
  2 NVIDIA A100 (40GB).

- Network ::
  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/scratch) of 4 TB (RAID0 of 4 HPE SATA-6G HDD @ 7.2krpm).

  BeeGFS over 10G Ethernet.

*** enbata01-02 with 2 AMD MI210 GPUs
:PROPERTIES:
:CUSTOM_ID: suet01
:END:

- CPU :: 2x 32-core AMD Zen4 Epyc 9334 @ 2.7 GHz.

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-enbata01.png]]

- Memory ::
  384GB (6GB/core) @ 4800 MT/s.

- GPUs ::
  2 AMD MI210 GPU (64GB).

- Network ::
  Mellanox InfiniBand HDR 200 Gbit/s.

  10 Gbit/s Ethernet.

- Storage ::
  BeeGFS over 10G Ethernet.

*** suet01 with 2 Intel Flex 170 GPUs
:PROPERTIES:
:CUSTOM_ID: suet01
:END:

- CPU :: 2x 28-core Intel IceLake Xeon Gold 6330 @ 2 GHz.

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-suet01.png]]

- Memory ::
  256GB (4.6GB/core) @ 3200 MT/s.

- GPUs ::
  2 Intel DataCenter GPU Flex 170 (16GB).

- Network ::
  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/scratch) of 1 TB (SATA HDD @ 7.2krpm).

  No BeeGFS over 10G Ethernet yet (@@html:<red>@@FIXME@@html:</red>@@).

*** kona01-04 Knights Landing Xeon Phi
:PROPERTIES:
:CUSTOM_ID: kona01-04
:END:

- CPU :: 64-core Intel Xeon Phi 7230 @ 1.3 GHz (4 hyperthreads per core).
  ([[https://en.wikichip.org/wiki/intel/microarchitectures/airmont][Airmont core specs]])

  By default, Turbo-Boost is disabled to ensure the reproducibility of the experiments carried out on the nodes.

  However Hyperthreading is enabled on these nodes.

- Memory ::
  96GB of DRAM (1.5GB/core) @ 2400 MT/s.

  16GB of MCDRAM on-package (0.25GB/core).

- Network ::
  Only 1 Gbit/s Ethernet.

  Omnipath 100 Gbit/s.

- Storage ::
  Local disk (/scratch) of 800 GB (SSD Intel SSDSC2BX80).

  BeeGFS over 1G Ethernet.

- KNL configuration ::

  kona01 is in Quadrant/Flat: 64 cores, 2 NUMA nodes for DRAM and MCDRAM.

  [[file:./figures/lstopo-kona01.png]]

  kona02 is in Quadrant/Cache: 64 cores, 1 NUMA node for DRAM with MCDRAM as a cache in front of it.

  [[file:./figures/lstopo-kona02.png]]

  kona03 is in SNC-4/Flat: 4 clusters with 16 cores and 2 NUMA nodes each.

  [[file:./figures/lstopo-kona03.png]]

  kona04 is in SNC-4/Cache: 4 clusters with 16 cores, 1 DRAM NUMA node and MCDRAM as a cache.

  [[file:./figures/lstopo-kona04.png]]

** Big Memory Nodes
:PROPERTIES:
:CUSTOM_ID: big_memory_nodes
:END:

Two nodes are specifically considered as Big Memory nodes : brise and souris which are described below.

Some other nodes could be also considered as Big Memory nodes : diablo05-09 and sirocco17 as they have 1 TB of memory as described above.

*** brise with 4 sockets, 96 cores and 1 TB memory
:PROPERTIES:
:CUSTOM_ID: brise
:END:

- CPU :: 4x 24-core Intel Xeon E7-8890 v4 @ 2.2GHz
  ([[https://en.wikichip.org/wiki/intel/xeon_e7/e7-8890_v4][CPU specs]]).

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-brise.png]]

- Memory ::
  1 TB (10.7 GB/core) @ 1600 MT/s.

- Network ::
  10 Gbit/s Ethernet.

- Storage ::
  Local disk (/tmp) of 280 GB (SAS @ 15krpm).

  BeeGFS over 10G Ethernet.

*** souris (SGI Altix UV2000) with 12 sockets, 96 cores and 3 TB memory
:PROPERTIES:
:CUSTOM_ID: souris
:END:

- CPU ::
  12x 8-core Intel Ivy-Bridge Xeon E5-4620 v2 @ 2.6 GHz ([[https://en.wikichip.org/wiki/intel/microarchitectures/ivy_bridge_(client)][Ivy Bridge specs]])

  By default, Turbo-Boost and Hyperthreading are disabled to ensure the reproducibility of the experiments carried out on the nodes.

  [[file:./figures/lstopo-souris.png]]

- Memory ::
  3TB (32GB/core) @ 1600MT/s.

- Network ::
  10 Gbit/s Ethernet.

- Storage ::
  No local storage.

  BeeGFS over 10G Ethernet.

