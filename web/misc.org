* Misc
:PROPERTIES:
:CUSTOM_ID: misc
:END:
** Citing PlaFRIM in your publications and in the HAL Open Archive
:PROPERTIES:
:CUSTOM_ID: misc_publications
:END:

Don’t forget to cite PlaFRIM in all publications presenting results or contents obtained or derived from the usage of PlaFRIM.

Here's what to insert in your paper acknowlegments section :

/Experiments presented in this paper were carried out using the PlaFRIM experimental testbed, supported by Inria, CNRS (LABRI and IMB), Université de Bordeaux, Bordeaux INP and Conseil Régional d’Aquitaine (see https://www.plafrim.fr/).

When you deposit a publication in the Hal Open Archive, please add plafrim in the Project/Collaboration field of the metadata.

You may also check the [[https://hal.inria.fr/PLAFRIM/][current list of publications registered in HAL]].

** Getting Help
:PROPERTIES:
:CUSTOM_ID: misc_help
:END:

- For community sharing, basic questions, etc. ::
  contact plafrim-users or use the Mattermost server.
- To exchange more widely about the platform usage ::
  contact your [[https://www.plafrim.fr/governance/][representative]] in the user committee.
- For technical problem (access, account, administration, modules, etc) ::
  open a ticket by contacting plafrim-support.

For more details and links, see the [[https://www.plafrim.fr/support/][private support page]] (requires login).
Links to these resources are also given in the invite message
when you open a SSH connection to PlaFRIM front-end nodes.

** Changing my password for the website plafrim.fr
:PROPERTIES:
:CUSTOM_ID: misc_password
:END:

This is the usual wordpress password change procedure.
- Go to http://www.plafrim.fr/wp-login.php
- Click on “Lost your password ?”
- Enter either your PlaFRIM username (your SSH login) or the email address you used to create your PlaFRIM account, and click on “Get New Password”
- Check your email inbox
- Click on the link (the longer one) proposed within this email
- Choose your new password
- Test you can connect at http://www.plafrim.fr/connection/

** Improving this documentation
:PROPERTIES:
:CUSTOM_ID: misc_improve
:END:

- Fork the repository https://gitlab.inria.fr/plafrim-users/doc and clone it on your machine.
- Update source files web/*.org and push changes to your fork.
- Open a pull request.
- Thanks!
