** Copying files outside PlaFRIM
:PROPERTIES:
:CUSTOM_ID: filesender
:END:

*** Using scp
If you just need to copy files from PlaFRIM to your laptop, using
~scp~ should be enough. Note that ~scp~ can only be used from your
local computer.
#+begin_src sh :eval never-export
scp myfile plafrim:
scp plafrim:myfile .
#+end_src
Note that [[https://plafrim-users.gitlabpages.inria.fr/doc/#irods][IRODS]] can also be used between PlaFRIM and the MCIA.
*** Using filesender
If you need to send huge files available on the PlaFRIM nodes, you can
also use a command-line client for [[https://filesender.renater.fr/]]

Here some explanations on how to install and to use it.

- From your local computer
  - go to https://filesender.renater.fr/?s=user. If you are not
    already connected to the service, connect first and go to the 'my
    profile' page by clicking on the rightmost icon in the upper menu.
  - If you do not already have a API secret, create one.
  - Click on the link 'Download Python CLI Client configuration' and
    save the file ~filesender.py.ini~ on your computer.
- On PlaFRIM, install the client
#+begin_src sh :eval never-export
mkdir -p ~/.filesender/ ~/bin/
#+end_src
copy the previously downloaded file in ~~/.filesender/filesender.py.ini~
#+begin_src sh :eval never-export
 wget https://filesender.renater.fr/clidownload.php -O ~/bin/filesender.py
 chmod u+x ~/bin/filesender.py
 python3 -m venv ~/bin/filesender
 source ~/bin/filesender/bin/activate
 pip3 install requests urllib3
#+end_src
- On PlaFRIM, use the client
#+begin_src sh :eval never-export
source ~/bin/filesender/bin/activate
filesender.py -r recipient@domain.email File_to_send [File_to_send ...]
deactivate
#+end_src
An email will be sent to ~recipient@domain.email~ with a link to
download the file.

